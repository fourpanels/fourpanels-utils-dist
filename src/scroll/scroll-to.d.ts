import { View } from '@nativescript/core';
import { ScrollToAnimation } from './scroll.model';
export declare function scrollInToView(targetElem: HTMLElement | View): void;
/**
 * If `behavior` is null, then there is no animation
 */
export declare function scrollInToBottom(targetElem: HTMLElement, animation?: ScrollToAnimation): void;
export declare function scrollInToTop(targetElem: HTMLElement, behavior?: ScrollBehavior): void;
//# sourceMappingURL=scroll-to.d.ts.map