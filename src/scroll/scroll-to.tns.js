import { ScrollView } from '@nativescript/core';
export function scrollInToView(targetElem) {
    const scrollElem = getFirstParentScrollElem(targetElem);
    if (!scrollElem) {
        return;
    }
    let firstScrollElemChild = null;
    scrollElem.eachChild(child => {
        firstScrollElemChild = child;
        return false;
    });
    const scrollPosition = targetElem.getLocationRelativeTo(firstScrollElemChild).y;
    scrollElem.scrollToVerticalOffset(scrollPosition, true);
}
function getFirstParentScrollElem(targetElem) {
    var elem = targetElem;
    while (elem.parent) {
        if (elem instanceof ScrollView) {
            return elem;
        }
        elem = elem.parent;
    }
}
// TODO:
export function scrollInToBottom(targetElem, behavior = 'smooth') {
}
// TODO: 
export function scrollInToTop(targetElem, behavior = 'smooth') {
}
//# sourceMappingURL=scroll-to.tns.js.map