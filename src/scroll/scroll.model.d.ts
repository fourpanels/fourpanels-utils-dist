export declare enum ScrollToAnimation {
    /**
     * The scrolling happens immediately without animation
     */
    None = "none",
    /**
     * The scrolling happens in a single jump.
     */
    Auto = "auto",
    /**
     * The scrolling animates smoothly.
     */
    Smooth = "smooth"
}
//# sourceMappingURL=scroll.model.d.ts.map