export declare const SCROLL_BLOCK_CLASS_NAME = "scroll-strategy__block";
export declare const SCROLL_HIDE_CLASS_NAME = "scroll-strategy__hide";
export declare function enableBodyScroll(): void;
export declare function disableBodyScroll(): void;
export declare function hideBodyScrollbar(): void;
export declare function isScrollBlocked(): boolean;
//# sourceMappingURL=block-scroll.d.ts.map