export const SCROLL_BLOCK_CLASS_NAME = 'scroll-strategy__block';
export const SCROLL_HIDE_CLASS_NAME = 'scroll-strategy__hide';
export function enableBodyScroll() {
    var _a, _b;
    if (!isScrollBlocked()) {
        return;
    }
    const documentElement = document.documentElement;
    documentElement.classList.remove(SCROLL_BLOCK_CLASS_NAME);
    documentElement.scrollTo({
        top: Number(((_a = documentElement.style.top.match(/\d+/)) === null || _a === void 0 ? void 0 : _a[0]) || 0),
        left: Number(((_b = documentElement.style.left.match(/\d+/)) === null || _b === void 0 ? void 0 : _b[0]) || 0),
    });
    documentElement.style.removeProperty('top');
    documentElement.style.removeProperty('left');
    documentElement.style.removeProperty('overflow');
}
export function disableBodyScroll() {
    if (isScrollBlocked()) {
        return;
    }
    const body = document.body;
    const documentElement = document.documentElement;
    const hasBodyScrolled = body.scrollHeight > window.innerHeight || body.scrollWidth > window.innerWidth;
    if (hasBodyScrolled) {
        const documentRect = documentElement.getBoundingClientRect();
        const top = -documentRect.top || document.body.scrollTop || window.scrollY || documentElement.scrollTop || 0;
        const left = -documentRect.left || document.body.scrollLeft || window.scrollX || documentElement.scrollLeft || 0;
        documentElement.style.top = -top + 'px';
        documentElement.style.left = -left + 'px';
        documentElement.classList.add(SCROLL_BLOCK_CLASS_NAME);
    }
}
export function hideBodyScrollbar() {
    const documentElement = document.documentElement;
    documentElement.style.overflow = 'hidden';
}
// isScrollBlocked -> isScrollDisabled
export function isScrollBlocked() {
    return document.documentElement.classList.contains(SCROLL_BLOCK_CLASS_NAME);
}
//# sourceMappingURL=block-scroll.js.map