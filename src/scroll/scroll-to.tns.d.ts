import { View } from '@nativescript/core';
export declare function scrollInToView(targetElem: View | HTMLElement): void;
export declare function scrollInToBottom(targetElem: HTMLElement, behavior?: ScrollBehavior): void;
export declare function scrollInToTop(targetElem: HTMLElement, behavior?: ScrollBehavior): void;
//# sourceMappingURL=scroll-to.tns.d.ts.map