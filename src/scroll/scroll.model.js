export var ScrollToAnimation;
(function (ScrollToAnimation) {
    /**
     * The scrolling happens immediately without animation
     */
    ScrollToAnimation["None"] = "none";
    /**
     * The scrolling happens in a single jump.
     */
    ScrollToAnimation["Auto"] = "auto";
    /**
     * The scrolling animates smoothly.
     */
    ScrollToAnimation["Smooth"] = "smooth";
})(ScrollToAnimation || (ScrollToAnimation = {}));
//# sourceMappingURL=scroll.model.js.map