import { ScrollToAnimation } from './scroll.model';
export function scrollInToView(targetElem) {
    targetElem.scrollIntoView({ block: 'center', behavior: 'smooth' });
}
/**
 * If `behavior` is null, then there is no animation
 */
export function scrollInToBottom(targetElem, animation = ScrollToAnimation.Smooth) {
    // If `scrollOption` is without behavior, then javascript will ignore scroll animation. `behavior: undefined` wil not work.
    const scrollOption = animation === ScrollToAnimation.None ?
        { top: targetElem.scrollHeight } :
        { top: targetElem.scrollHeight, behavior: animation };
    targetElem.scroll(scrollOption);
}
export function scrollInToTop(targetElem, behavior = 'smooth') {
    targetElem.scroll({ top: 0, behavior });
}
//# sourceMappingURL=scroll-to.js.map