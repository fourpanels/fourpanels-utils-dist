// y1 -> x2
// x2 -> y1
import { isNumber } from '../ensure/ensure';
export function calcLinearEquation({ x, x1, y1, x2, y2 }) {
    if (x1 === x2) {
        return y1;
    }
    const m = (y1 - y2) / (x1 - x2);
    const b = y1 - (m * x1);
    const y = (m * x) + b;
    return y;
}
/**
 * Returns `value` if `value` is between `min` and `max`.
 * Returns `min` if `value` is lower than `min`.
 * Returns `max` if `value` is higher than `min`.
 */
export function clamp(value, min, max) {
    return Math.max(min, Math.min(value, max));
}
/**
 * Returns `value` if `value` is a of type Number otherwise `defaultValue`.
 */
export function toNumberOrDefault(value, defaultValue) {
    return isNumber(Number(value)) ? Number(value) : defaultValue;
}
export function sum(a, b) {
    return a + b;
}
//# sourceMappingURL=math.js.map