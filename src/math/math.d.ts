export declare function calcLinearEquation({ x, x1, y1, x2, y2 }: {
    x: number;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}): number;
/**
 * Returns `value` if `value` is between `min` and `max`.
 * Returns `min` if `value` is lower than `min`.
 * Returns `max` if `value` is higher than `min`.
 */
export declare function clamp(value: number, min: number, max: number): number;
/**
 * Returns `value` if `value` is a of type Number otherwise `defaultValue`.
 */
export declare function toNumberOrDefault(value: any, defaultValue: number): number;
export declare function sum(a: number, b: number): number;
//# sourceMappingURL=math.d.ts.map