import { clamp } from "../math/math";
import { fetchScript } from "./fetch-files";
const PDF_SRC_URL = 'assets/pdf/pdf.min.js';
/**
 * How it works:
 *
 * 1- Load pdf.js script from the server and set it in the project.
 *    On `loadPdf` methods, we have to check if pdf.js has already been installed. If no,
 *    then load it from the server and install it. (`loadPdfJsFromServer`)
 *
 * 2- Parse pdf source file and save it as `pdfDoc`.  Other code will need it later. (`parsePdf` method)
 *
 * 3- Parse target pdf page number and save it in `pages`. (`getPage` method)
 *
 * 4- Create a new canvas from the selected page. (`getCanvasOfPage` method)
 *
 * 5- Export image (Base64 or Blob) from canvas. (`getPageAsBase64` method)
 *
 *
 * How to use it:
 *
 * const pdf = new PdfJs();
 * await pdf.loadPdf(fileSource);
 * const image = await pdf.getPageAsBase64(1);
 */
export class PdfJs {
    constructor() {
        this.pages = {};
    }
    // --- Public Methods ---
    async loadPdf(pdfFile) {
        if (!window.pdfjsLib) {
            await PdfJs.loadPdfJsFromServer();
        }
        await this.parsePdf(pdfFile);
    }
    async getPageAsBase64(pageNumber, pageScale = 1) {
        const clampPageNumber = this.clampPageNumber(pageNumber);
        const canvas = await this.getCanvasOfPage(clampPageNumber, pageScale);
        return canvas.toDataURL('image/jpeg');
    }
    async getPageAsBlob(pageNumber, pageScale = 1) {
        const clampPageNumber = this.clampPageNumber(pageNumber);
        const canvas = await this.getCanvasOfPage(clampPageNumber, pageScale);
        return new Promise((r) => canvas.toBlob((blob) => r(blob), 'image/jpeg'));
    }
    // --- Private Methods ---
    static async loadPdfJsFromServer() {
        // await fetchScript(PDF_SRC_URL).toPromise();
        await fetchScript(PDF_SRC_URL);
    }
    async parsePdf(pdfFile) {
        // const pdfSrc = await fileToArrayBuffer(pdfFile).toPromise();
        const pdfSrc = await pdfFile.arrayBuffer();
        this.pdfDoc = await pdfjsLib.getDocument(pdfSrc);
    }
    async getPage(pageNumber) {
        if (!this.pages[pageNumber]) {
            this.pages[pageNumber] =
                await this.pdfDoc.getPage(pageNumber);
        }
        return this.pages[pageNumber];
    }
    async getCanvasOfPage(pageNumber, pageScale) {
        const page = await this.getPage(pageNumber);
        const viewport = page.getViewport({ scale: pageScale });
        // Create canvas element
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        // Render PDF page into canvas context
        const task = page.render({ canvasContext: context, viewport });
        await task.promise;
        return canvas;
    }
    clampPageNumber(pageNumber) {
        return clamp(pageNumber, 1, this.pdfDoc.numPages);
    }
}
//# sourceMappingURL=pdf.js.map