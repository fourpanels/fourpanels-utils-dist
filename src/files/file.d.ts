/**
 * Create FileReader and use `readAsArrayBuffer` method to get ArrayBuffer of file
 */
export declare function fileToArrayBuffer(file: File): import("rxjs").Observable<string | ArrayBuffer>;
//# sourceMappingURL=file.d.ts.map