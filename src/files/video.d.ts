/**
 *
 * @param videoFile the video file
 * @param mimeType DOMString indicating the image format (optional). The default type is image/png.
 * @param quality A number between 0 and 1 indicating image quality (optional). Only used if requested type is image/jpeg or image/webp. If this argument is anything else, the default values 0.92 and 0.80 are used for image/jpeg and image/webp respectively.
 * @returns Promise<Blob | null>; If Promise resolves to a Blob it contains the snapshot of the video at the middle
 */
export declare function getVideoSnapshotAsBlob(videoFile: File, mimeType?: string, quality?: number): Promise<Blob | null>;
/**
 *
 * @param videoFile the video file
 * @param mimeType DOMString indicating the image format (optional). The default type is image/png.
 * @param quality A number between 0 and 1 indicating image quality (optional). Only used if requested type is image/jpeg or image/webp. If this argument is anything else, the default values 0.92 and 0.80 are used for image/jpeg and image/webp respectively.
 * @returns Promise<string> resolving to snapshot of the video at the middle of the video as DataURL
 */
export declare function getVideoSnapshotAsDataURL(videoFile: File, mimeType?: string, quality?: number): Promise<string>;
//# sourceMappingURL=video.d.ts.map