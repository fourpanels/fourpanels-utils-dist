/**
 * Fetches a script from provided URL and adds it to project.
 */
export function fetchScript(url) {
    const script = document.createElement('script');
    document.body.appendChild(script);
    script.type = 'text/javascript';
    script.async = true;
    script.src = url;
    return new Promise((r) => script.onload = () => r());
}
/**
 * Fetches a stylesheet from provided URL and adds to project.
 */
export function fetchStyle(url) {
    const style = document.createElement('link');
    document.getElementsByTagName('head')[0].appendChild(style);
    style.type = 'text/css';
    style.href = url;
    style.rel = 'stylesheet';
    return new Promise((r) => style.onload = () => r());
}
// export function fetchScript(url: string): Observable<void> {
//
//     const result$ = new ReplaySubject<void>();
//
//     const script = document.createElement('script');
//     script.type = 'text/javascript';
//     script.async = true;
//     script.src = url;
//
//     script.onload = () => {
//         result$.next();
//         result$.complete();
//     };
//
//     document.body.appendChild(script);
//
//     return result$.asObservable();
// }
// export function fetchStyle(url: string): Promise<void> {
//
//     const result$ = new ReplaySubject<void>();
//
//     const style = document.createElement('link');
//     style.type = 'text/css';
//     style.href = url;
//     style.rel = 'stylesheet';
//     style.onload = () => {
//         result$.next();
//         result$.complete();
//     };
//
//     const head = document.getElementsByTagName('head')[0];
//     head.appendChild(style);
//
//     return result$.toPromise();
// }
//# sourceMappingURL=fetch-files.js.map