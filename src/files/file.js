import { Subject } from "rxjs";
import { isNotNullish } from "../ensure/ensure";
/**
 * Create FileReader and use `readAsArrayBuffer` method to get ArrayBuffer of file
 */
// TODO Better use: file.arrayBuffer
export function fileToArrayBuffer(file) {
    const result$ = new Subject();
    const fileReader = new FileReader();
    fileReader.onload = (ev) => {
        if (isNotNullish(fileReader.result)) {
            result$.next(fileReader.result);
            result$.complete();
        }
    };
    fileReader.readAsArrayBuffer(file);
    return result$.asObservable();
}
//# sourceMappingURL=file.js.map