import { Subject } from 'rxjs';
/**
 *
 * @param contentType A unique file type specifier is a string that describes a type of file that may be selected by the user in an <input> element of type file.
 * @example
 * 1- '.jpg', '.pdf', '.doc' // valid case-insensitive filename extension
 * 2- 'application/msword', 'application/pdf' // MIME type
 * 3- 'audio/*' // meaning "any audio file".
 * 4- 'video/*' // meaning "any video file".
 * 5- 'image/*' // any image file.
 *
 * @param multiple A Boolean which, if present, indicates that the user may choose more than one file
 */
export function selectLocalFile$({ contentType = '', multiple = true }) {
    const result$ = new Subject();
    const onChange = () => {
        if (input.files) {
            result$.next(input.files);
        }
        result$.complete();
    };
    const input = document.createElement("input");
    input.type = "file";
    input.multiple = multiple;
    input.accept = contentType;
    input.onchange = onChange;
    input.click();
    return result$.asObservable();
}
//# sourceMappingURL=select-file.js.map