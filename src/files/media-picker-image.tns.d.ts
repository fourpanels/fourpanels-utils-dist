import { ImageSource } from "@nativescript/core";
import { Observable } from "rxjs";
import * as imagepicker from "@nativescript/imagepicker";
export declare function selectImageFromGalley$(options?: imagepicker.Options): Observable<ImageSource[]>;
//# sourceMappingURL=media-picker-image.tns.d.ts.map