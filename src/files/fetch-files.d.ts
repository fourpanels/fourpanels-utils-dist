/**
 * Fetches a script from provided URL and adds it to project.
 */
export declare function fetchScript(url: string): Promise<void>;
/**
 * Fetches a stylesheet from provided URL and adds to project.
 */
export declare function fetchStyle(url: string): Promise<void>;
//# sourceMappingURL=fetch-files.d.ts.map