import { Observable } from 'rxjs';
import { File } from '@nativescript/core';
export declare enum MediaPickerFileExtension {
    Pdf = 0,
    Text = 1
}
export interface MediaPickerFileOptions {
    extensions?: MediaPickerFileExtension[];
    androidMaxNumberFiles?: number;
    iosMultipleSelection?: boolean;
}
export declare function selectFileFromDevice$(options?: MediaPickerFileOptions): Observable<File[]>;
//# sourceMappingURL=media-picker-file.d.ts.map