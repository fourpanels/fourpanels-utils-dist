/**
 *
 * @param videoFile the video file
 * @param mimeType DOMString indicating the image format (optional). The default type is image/png.
 * @param quality A number between 0 and 1 indicating image quality (optional). Only used if requested type is image/jpeg or image/webp. If this argument is anything else, the default values 0.92 and 0.80 are used for image/jpeg and image/webp respectively.
 * @returns Promise<Blob | null>; If Promise resolves to a Blob it contains the snapshot of the video at the middle
 */
export async function getVideoSnapshotAsBlob(videoFile, mimeType = 'image/jpeg', quality) {
    const canvas = await createCanvasWithSnapshotFromVideoFile(videoFile);
    return canvasToBlob(canvas, mimeType, quality);
}
/**
 *
 * @param videoFile the video file
 * @param mimeType DOMString indicating the image format (optional). The default type is image/png.
 * @param quality A number between 0 and 1 indicating image quality (optional). Only used if requested type is image/jpeg or image/webp. If this argument is anything else, the default values 0.92 and 0.80 are used for image/jpeg and image/webp respectively.
 * @returns Promise<string> resolving to snapshot of the video at the middle of the video as DataURL
 */
export async function getVideoSnapshotAsDataURL(videoFile, mimeType = 'image/jpeg', quality) {
    const canvas = await createCanvasWithSnapshotFromVideoFile(videoFile);
    return canvasToDataUrl(canvas, mimeType, quality);
}
async function createCanvasWithSnapshotFromVideoFile(videoFile) {
    const videoElem = await createVideoElem(videoFile);
    await jumpToMiddleOfVideo(videoElem);
    const canvas = createCanvasFromVideoElem(videoElem);
    drawCurrentVideoSnapshotOntoCanvas(videoElem, canvas);
    return canvas;
}
function createVideoElem(videoFile) {
    const videoElem = document.createElement('video');
    const sourceElem = document.createElement('source');
    videoElem.style.position = 'absolute';
    videoElem.style.opacity = '0';
    sourceElem.setAttribute('src', URL.createObjectURL(videoFile));
    videoElem.appendChild(sourceElem);
    return new Promise(resolve => videoElem.addEventListener('loadeddata', () => resolve(videoElem), { once: true }));
}
function createCanvasFromVideoElem(videoElem) {
    const canvas = document.createElement('canvas');
    canvas.width = videoElem.videoWidth;
    canvas.height = videoElem.videoHeight;
    return canvas;
}
function jumpToMiddleOfVideo(video) {
    return new Promise(resolve => {
        video.currentTime = video.duration / 2;
        video.addEventListener('seeked', () => resolve(), { once: true });
    });
}
function drawCurrentVideoSnapshotOntoCanvas(videoElem, canvas) {
    const videoContext = canvas.getContext('2d');
    videoContext === null || videoContext === void 0 ? void 0 : videoContext.drawImage(videoElem, 0, 0, videoElem.videoWidth, videoElem.videoHeight);
}
function canvasToBlob(canvas, mimeType, quality) {
    return new Promise(resolve => canvas.toBlob(resolve, mimeType, quality));
}
function canvasToDataUrl(canvas, mimeType, quality) {
    return canvas.toDataURL(mimeType, quality);
}
//# sourceMappingURL=video.js.map