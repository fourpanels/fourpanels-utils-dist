import PdfjsLib from "./pdfjs";
declare global {
    interface Window {
        pdfjsLib: typeof PdfjsLib;
    }
    const pdfjsLib: typeof PdfjsLib;
}
/**
 * How it works:
 *
 * 1- Load pdf.js script from the server and set it in the project.
 *    On `loadPdf` methods, we have to check if pdf.js has already been installed. If no,
 *    then load it from the server and install it. (`loadPdfJsFromServer`)
 *
 * 2- Parse pdf source file and save it as `pdfDoc`.  Other code will need it later. (`parsePdf` method)
 *
 * 3- Parse target pdf page number and save it in `pages`. (`getPage` method)
 *
 * 4- Create a new canvas from the selected page. (`getCanvasOfPage` method)
 *
 * 5- Export image (Base64 or Blob) from canvas. (`getPageAsBase64` method)
 *
 *
 * How to use it:
 *
 * const pdf = new PdfJs();
 * await pdf.loadPdf(fileSource);
 * const image = await pdf.getPageAsBase64(1);
 */
export declare class PdfJs {
    private pdfDoc;
    private pages;
    loadPdf(pdfFile: File): Promise<void>;
    getPageAsBase64(pageNumber: number, pageScale?: number): Promise<string>;
    getPageAsBlob(pageNumber: number, pageScale?: number): Promise<Blob | null>;
    private static loadPdfJsFromServer;
    private parsePdf;
    private getPage;
    private getCanvasOfPage;
    private clampPageNumber;
}
//# sourceMappingURL=pdf.d.ts.map