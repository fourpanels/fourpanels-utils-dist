import { File, isIOS } from "@nativescript/core";
import { Mediafilepicker } from "nativescript-mediafilepicker";
import { Subject } from "rxjs";
export var MediaPickerFileExtension;
(function (MediaPickerFileExtension) {
    MediaPickerFileExtension[MediaPickerFileExtension["Pdf"] = 0] = "Pdf";
    MediaPickerFileExtension[MediaPickerFileExtension["Text"] = 1] = "Text";
})(MediaPickerFileExtension || (MediaPickerFileExtension = {}));
export const MediaPickerFileExtensionValue = {
    /**
     * We create default extensions file config for the mediaFilePicker plugin. But each device knows his extension type.
     * If we try to access IOS extensions type in android (i.e kUTTypePDF, kUTTypeText), then we receive an error. (i.e. kUTTypePDF in not defined)
     * IOS and no problem to get the reference of android type, because it is a string.
     *
     * So we have to set ios extensions file, just if the device is ios.
     */
    [MediaPickerFileExtension.Pdf]: {
        ios: isIOS ? kUTTypePDF : null,
        android: 'pdf'
    },
    [MediaPickerFileExtension.Text]: {
        ios: isIOS ? kUTTypeText : null,
        android: 'txt'
    }
};
export function selectFileFromDevice$(options) {
    var _a, _b, _c, _d, _e, _f;
    const result$ = new Subject();
    const allAndroidExtensions = [
        MediaPickerFileExtensionValue[MediaPickerFileExtension.Pdf].android,
        MediaPickerFileExtensionValue[MediaPickerFileExtension.Text].android,
    ];
    const allIosExtensions = [
        MediaPickerFileExtensionValue[MediaPickerFileExtension.Pdf].ios,
        MediaPickerFileExtensionValue[MediaPickerFileExtension.Text].ios,
    ];
    const finalOptions = {
        android: {
            extensions: (_b = (_a = options === null || options === void 0 ? void 0 : options.extensions) === null || _a === void 0 ? void 0 : _a.map(extensionType => MediaPickerFileExtensionValue[extensionType].android)) !== null && _b !== void 0 ? _b : allAndroidExtensions,
            maxNumberFiles: (_c = options === null || options === void 0 ? void 0 : options.androidMaxNumberFiles) !== null && _c !== void 0 ? _c : 10,
        }, ios: {
            extensions: (_e = (_d = options === null || options === void 0 ? void 0 : options.extensions) === null || _d === void 0 ? void 0 : _d.map(extensionType => MediaPickerFileExtensionValue[extensionType].ios)) !== null && _e !== void 0 ? _e : allIosExtensions,
            multipleSelection: (_f = options === null || options === void 0 ? void 0 : options.iosMultipleSelection) !== null && _f !== void 0 ? _f : true,
        }
    };
    const mediafilepicker = new Mediafilepicker();
    mediafilepicker.openFilePicker(finalOptions);
    mediafilepicker.on("getFiles", function (res) {
        const results = res.object.get('results');
        const filePaths = results.map(result => result.file.replace('file:///', ''));
        const files = filePaths.map(path => File.fromPath(path));
        result$.next(files);
        result$.complete();
    });
    mediafilepicker.on("error", function (res) {
        const msg = res.object.get('msg');
        console.log('Error on file picker', msg);
        result$.complete();
    });
    mediafilepicker.on("cancel", function (res) {
        result$.complete();
    });
    return result$.asObservable();
}
//# sourceMappingURL=media-picker-file.tns.js.map