import { File } from "@nativescript/core";
import { Observable } from "rxjs";
/**
 * For more IOS File Type see this link.
 * https://developer.apple.com/documentation/mobilecoreservices/uttype
 */
declare var kUTTypePDF: any;
declare type kUTTypePDF = any;
declare var kUTTypeText: any;
declare type kUTTypeText = any;
export declare enum MediaPickerFileExtension {
    Pdf = 0,
    Text = 1
}
export interface MediaPickerFileExtensionPlatformType {
    ios: kUTTypeText | kUTTypePDF;
    android: 'pdf' | 'txt';
}
export declare const MediaPickerFileExtensionValue: Record<MediaPickerFileExtension, MediaPickerFileExtensionPlatformType>;
export interface MediaPickerFileOptions {
    extensions?: MediaPickerFileExtension[];
    androidMaxNumberFiles?: number;
    iosMultipleSelection?: boolean;
}
export declare function selectFileFromDevice$(options?: MediaPickerFileOptions): Observable<File[]>;
export {};
//# sourceMappingURL=media-picker-file.tns.d.ts.map