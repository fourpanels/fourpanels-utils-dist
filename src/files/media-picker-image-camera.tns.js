import { ImageSource } from "@nativescript/core";
import * as camera from "@nativescript/camera";
import { from } from "rxjs";
import { switchMap } from "rxjs/operators";
export function takeImageFromCamera$(options) {
    return from(camera.requestPermissions()).pipe(switchMap(_ => camera.takePicture(options)), switchMap(imageAsset => ImageSource.fromAsset(imageAsset)));
}
//# sourceMappingURL=media-picker-image-camera.tns.js.map