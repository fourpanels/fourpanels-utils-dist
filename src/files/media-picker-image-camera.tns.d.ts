import { ImageSource } from "@nativescript/core";
import * as camera from "@nativescript/camera";
import { Observable } from "rxjs";
export declare function takeImageFromCamera$(options?: camera.CameraOptions): Observable<ImageSource>;
//# sourceMappingURL=media-picker-image-camera.tns.d.ts.map