import { ImageSource } from "@nativescript/core";
import { from } from "rxjs";
import * as imagepicker from "@nativescript/imagepicker";
import { switchMap } from "rxjs/operators";
const DEFAULT_OPTION = {
    mode: "multiple",
    mediaType: 1 /* Image */
};
export function selectImageFromGalley$(options) {
    const context = imagepicker.create(Object.assign(Object.assign({}, DEFAULT_OPTION), options));
    return from(context.authorize()).pipe(switchMap(_ => context.present()), switchMap(selectedImageAsset => Promise.all(selectedImageAsset.map(a => ImageSource.fromAsset(a)))));
}
//# sourceMappingURL=media-picker-image.tns.js.map