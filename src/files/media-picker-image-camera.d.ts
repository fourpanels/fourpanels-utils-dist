import * as camera from '@nativescript/camera';
import { ImageSource } from '@nativescript/core';
import { Observable } from 'rxjs';
export declare function takeImageFromCamera$(options?: camera.CameraOptions): Observable<ImageSource>;
//# sourceMappingURL=media-picker-image-camera.d.ts.map