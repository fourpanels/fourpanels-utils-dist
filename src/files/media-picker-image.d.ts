import * as imagepicker from '@nativescript/imagepicker';
import { Observable } from 'rxjs';
import { ImageSource } from '@nativescript/core/image-source';
export declare function selectImageFromGalley$(options?: imagepicker.Options): Observable<ImageSource[]>;
//# sourceMappingURL=media-picker-image.d.ts.map