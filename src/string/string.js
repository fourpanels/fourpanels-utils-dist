// Source from: https://www.w3resource.com/javascript-exercises/javascript-math-exercise-23.php
export function create_UUID() {
    const dataNow = new Date().getTime();
    const uuidPattern = 'xxxxx-xxxxx-xxxxx-xxxxx-xxxxx';
    const replaceXToNumber = () => ((dataNow + Math.random() * 16) % 16 | 0).toString(16);
    return uuidPattern.replace(/[x]/g, replaceXToNumber);
}
export function firstCharUpperCase(value) {
    let result = value.toLowerCase();
    result = result.substr(0, 1).toUpperCase() + result.substr(1);
    return result;
}
//# sourceMappingURL=string.js.map