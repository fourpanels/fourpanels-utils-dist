import { Predicate } from '../array/array.model';
export declare function not<T>(fn: Predicate<T>): Predicate<T>;
export declare function and<T>(pred1: Predicate<T>, pred2: Predicate<T>): Predicate<T>;
export declare function or<T>(pred1: Predicate<T>, pred2: Predicate<T>): Predicate<T>;
//# sourceMappingURL=functions.d.ts.map