export function not(fn) {
    return function (value) {
        return !fn(value);
    };
}
export function and(pred1, pred2) {
    return function (value) {
        return pred1(value) && pred2(value);
    };
}
export function or(pred1, pred2) {
    return function (value) {
        return pred1(value) || pred2(value);
    };
}
//# sourceMappingURL=functions.js.map