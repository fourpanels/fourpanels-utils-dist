import { NgZone } from '@angular/core';
import { OperatorFunction } from 'rxjs';
/**
 * Copy from stackoverflow:
 * https://stackoverflow.com/a/57452361/1538285
 */
/**
 * TODO (Milad / Util / Later): runInsideZone pipe don't work. i don't know why
 * but i test it in `lib-bottom-sheet-modal.component.ts/ngOnInit/this.animationService.openChange$`
 * and template will not updated
 */
export declare function runInsideZone<T>(zone: NgZone): OperatorFunction<T, T>;
//# sourceMappingURL=run-inside-zone.d.ts.map