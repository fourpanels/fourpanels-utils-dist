import { Observable } from 'rxjs';
/**
 * Copy from stackoverflow:
 * https://stackoverflow.com/a/57452361/1538285
 */
/**
 * TODO (Milad / Util / Later): runInsideZone pipe don't work. i don't know why
 * but i test it in `lib-bottom-sheet-modal.component.ts/ngOnInit/this.animationService.openChange$`
 * and template will not updated
 */
export function runInsideZone(zone) {
    return (source) => {
        return new Observable(observer => {
            const onNext = (value) => zone.run(() => observer.next(value));
            const onError = (e) => zone.run(() => observer.error(e));
            const onComplete = () => zone.run(() => observer.complete());
            return source.subscribe(onNext, onError, onComplete);
        });
    };
}
//# sourceMappingURL=run-inside-zone.js.map