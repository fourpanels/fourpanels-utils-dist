import { Observable } from "rxjs";
import { share } from "rxjs/operators";
export function mutationObserver$(elem, config) {
    return new Observable(subscriber => {
        const callBack = (entries, observer) => subscriber.next({ entries, observer });
        const onUnSubscribe = () => observer.disconnect();
        const observer = new MutationObserver(callBack);
        observer.observe(elem, config);
        return onUnSubscribe;
    }).pipe(share());
}
//# sourceMappingURL=mutation-observer.js.map