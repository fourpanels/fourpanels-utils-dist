export interface MutationObserverConfig extends MutationObserverInit {
}
export interface MutationObserverChanges {
    entries: MutationRecord[];
    observer: MutationObserver;
}
//# sourceMappingURL=mutation-observer.model.d.ts.map