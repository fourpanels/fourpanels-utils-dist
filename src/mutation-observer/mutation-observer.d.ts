import { Observable } from "rxjs";
import { MutationObserverChanges, MutationObserverConfig } from "./mutation-observer.model";
export declare function mutationObserver$(elem: HTMLElement, config: MutationObserverConfig): Observable<MutationObserverChanges>;
//# sourceMappingURL=mutation-observer.d.ts.map