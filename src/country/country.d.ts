import { CountryCode, SubdivisionCode, SubdivisionEntry } from './country.model';
export declare function getCountryNameFromCode(countryCode: CountryCode): string;
export declare function getSubdivisionNameFromCode(subdivisionCode: SubdivisionCode): string;
export declare function getSubdivisionListFromCountry(countryCode: CountryCode): SubdivisionEntry[];
//# sourceMappingURL=country.d.ts.map