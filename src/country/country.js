import { ISO_COUNTRIES_MAP, SUBDIVISION_COUNTRY_MAP, SUBDIVISION_STATE_MAP } from './country.db';
export function getCountryNameFromCode(countryCode) {
    return ISO_COUNTRIES_MAP[countryCode];
}
export function getSubdivisionNameFromCode(subdivisionCode) {
    return SUBDIVISION_STATE_MAP[subdivisionCode];
}
export function getSubdivisionListFromCountry(countryCode) {
    return SUBDIVISION_COUNTRY_MAP[countryCode];
}
//# sourceMappingURL=country.js.map