import { SubdivisionCode } from './country.model';
export declare const ISO_COUNTRIES_LIST: {
    code: string;
    name: string;
}[];
export declare const ISO_COUNTRIES_MAP: {
    AF: string;
    AX: string;
    AL: string;
    DZ: string;
    AS: string;
    AD: string;
    AO: string;
    AI: string;
    AQ: string;
    AG: string;
    AR: string;
    AM: string;
    AW: string;
    AU: string;
    AT: string;
    AZ: string;
    BS: string;
    BH: string;
    BD: string;
    BB: string;
    BY: string;
    BE: string;
    BZ: string;
    BJ: string;
    BM: string;
    BT: string;
    BQ: string;
    BO: string;
    BA: string;
    BW: string;
    BV: string;
    BR: string;
    IO: string;
    BN: string;
    BG: string;
    BF: string;
    BI: string;
    KH: string;
    CM: string;
    CA: string;
    CW: string;
    CV: string;
    KY: string;
    CF: string;
    TD: string;
    CL: string;
    CN: string;
    CX: string;
    CC: string;
    CO: string;
    KM: string;
    CG: string;
    CD: string;
    CK: string;
    CR: string;
    CI: string;
    HR: string;
    CU: string;
    CY: string;
    CZ: string;
    DK: string;
    DJ: string;
    DM: string;
    DO: string;
    EC: string;
    EG: string;
    SV: string;
    SS: string;
    SX: string;
    GQ: string;
    ER: string;
    EE: string;
    ET: string;
    FK: string;
    FO: string;
    FJ: string;
    FI: string;
    FR: string;
    GF: string;
    PF: string;
    TF: string;
    GA: string;
    GM: string;
    GE: string;
    DE: string;
    GH: string;
    GI: string;
    GR: string;
    GL: string;
    GD: string;
    GP: string;
    GU: string;
    GT: string;
    GG: string;
    GN: string;
    GW: string;
    GY: string;
    HT: string;
    HM: string;
    VA: string;
    HN: string;
    HK: string;
    HU: string;
    IS: string;
    IN: string;
    ID: string;
    IR: string;
    IQ: string;
    IE: string;
    IM: string;
    IL: string;
    IT: string;
    JM: string;
    JP: string;
    JE: string;
    JO: string;
    KZ: string;
    KE: string;
    KI: string;
    KR: string;
    KP: string;
    KW: string;
    KG: string;
    LA: string;
    LV: string;
    LB: string;
    LS: string;
    LR: string;
    LY: string;
    LI: string;
    LT: string;
    LU: string;
    MO: string;
    MK: string;
    MG: string;
    MW: string;
    MY: string;
    MV: string;
    ML: string;
    MT: string;
    MH: string;
    MQ: string;
    MR: string;
    MU: string;
    YT: string;
    MX: string;
    FM: string;
    MD: string;
    MC: string;
    MN: string;
    ME: string;
    MS: string;
    MA: string;
    MZ: string;
    MM: string;
    NA: string;
    NR: string;
    NP: string;
    NL: string;
    AN: string;
    NC: string;
    NZ: string;
    NI: string;
    NE: string;
    NG: string;
    NU: string;
    NF: string;
    MP: string;
    NO: string;
    OM: string;
    PK: string;
    PW: string;
    PS: string;
    PA: string;
    PG: string;
    PY: string;
    PE: string;
    PH: string;
    PN: string;
    PL: string;
    PT: string;
    PR: string;
    QA: string;
    RE: string;
    RO: string;
    RU: string;
    RW: string;
    BL: string;
    SH: string;
    KN: string;
    LC: string;
    MF: string;
    PM: string;
    VC: string;
    WS: string;
    SM: string;
    ST: string;
    SA: string;
    SN: string;
    RS: string;
    SC: string;
    SL: string;
    SG: string;
    SK: string;
    SI: string;
    SB: string;
    SO: string;
    ZA: string;
    GS: string;
    ES: string;
    LK: string;
    SD: string;
    SR: string;
    SJ: string;
    SZ: string;
    SE: string;
    CH: string;
    SY: string;
    TW: string;
    TJ: string;
    TZ: string;
    TH: string;
    TL: string;
    TG: string;
    TK: string;
    TO: string;
    TT: string;
    TN: string;
    TR: string;
    TM: string;
    TC: string;
    TV: string;
    UG: string;
    UA: string;
    AE: string;
    GB: string;
    US: string;
    UM: string;
    UY: string;
    UZ: string;
    VU: string;
    VE: string;
    VN: string;
    VG: string;
    VI: string;
    WF: string;
    EH: string;
    YE: string;
    ZM: string;
    ZW: string;
};
export declare const SUBDIVISION_COUNTRY_MAP: {
    AD: {
        code: string;
        name: string;
    }[];
    AE: {
        code: string;
        name: string;
    }[];
    AF: {
        code: string;
        name: string;
    }[];
    AG: {
        code: string;
        name: string;
    }[];
    AI: never[];
    AL: {
        code: string;
        name: string;
    }[];
    AM: {
        code: string;
        name: string;
    }[];
    AO: {
        code: string;
        name: string;
    }[];
    AQ: never[];
    AR: {
        code: string;
        name: string;
    }[];
    AS: never[];
    AT: {
        code: string;
        name: string;
    }[];
    AU: {
        code: string;
        name: string;
    }[];
    AW: never[];
    AX: never[];
    AZ: {
        code: string;
        name: string;
    }[];
    BA: {
        code: string;
        name: string;
    }[];
    BB: {
        code: string;
        name: string;
    }[];
    BD: {
        code: string;
        name: string;
    }[];
    BE: {
        code: string;
        name: string;
    }[];
    BF: {
        code: string;
        name: string;
    }[];
    BG: {
        code: string;
        name: string;
    }[];
    BH: {
        code: string;
        name: string;
    }[];
    BI: {
        code: string;
        name: string;
    }[];
    BJ: {
        code: string;
        name: string;
    }[];
    BL: never[];
    BM: never[];
    BN: {
        code: string;
        name: string;
    }[];
    BO: {
        code: string;
        name: string;
    }[];
    BQ: {
        code: string;
        name: string;
    }[];
    BR: {
        code: string;
        name: string;
    }[];
    BS: {
        code: string;
        name: string;
    }[];
    BT: {
        code: string;
        name: string;
    }[];
    BV: never[];
    BW: {
        code: string;
        name: string;
    }[];
    BY: {
        code: string;
        name: string;
    }[];
    BZ: {
        code: string;
        name: string;
    }[];
    CA: {
        code: string;
        name: string;
    }[];
    CC: never[];
    CD: {
        code: string;
        name: string;
    }[];
    CF: {
        code: string;
        name: string;
    }[];
    CG: {
        code: string;
        name: string;
    }[];
    CH: {
        code: string;
        name: string;
    }[];
    CI: {
        code: string;
        name: string;
    }[];
    CK: never[];
    CL: {
        code: string;
        name: string;
    }[];
    CM: {
        code: string;
        name: string;
    }[];
    CN: {
        code: string;
        name: string;
    }[];
    CO: {
        code: string;
        name: string;
    }[];
    CR: {
        code: string;
        name: string;
    }[];
    CU: {
        code: string;
        name: string;
    }[];
    CV: {
        code: string;
        name: string;
    }[];
    CW: never[];
    CX: never[];
    CY: {
        code: string;
        name: string;
    }[];
    CZ: {
        code: string;
        name: string;
    }[];
    DE: {
        code: string;
        name: string;
    }[];
    DJ: {
        code: string;
        name: string;
    }[];
    DK: {
        code: string;
        name: string;
    }[];
    DM: {
        code: string;
        name: string;
    }[];
    DO: {
        code: string;
        name: string;
    }[];
    DZ: {
        code: string;
        name: string;
    }[];
    EC: {
        code: string;
        name: string;
    }[];
    EE: {
        code: string;
        name: string;
    }[];
    EG: {
        code: string;
        name: string;
    }[];
    EH: never[];
    ER: {
        code: string;
        name: string;
    }[];
    ES: {
        code: string;
        name: string;
    }[];
    ET: {
        code: string;
        name: string;
    }[];
    FI: {
        code: string;
        name: string;
    }[];
    FJ: {
        code: string;
        name: string;
    }[];
    FK: never[];
    FM: {
        code: string;
        name: string;
    }[];
    FO: never[];
    FR: {
        code: string;
        name: string;
    }[];
    GA: {
        code: string;
        name: string;
    }[];
    GB: {
        code: string;
        name: string;
    }[];
    GD: {
        code: string;
        name: string;
    }[];
    GE: {
        code: string;
        name: string;
    }[];
    GF: never[];
    GG: never[];
    GH: {
        code: string;
        name: string;
    }[];
    GI: never[];
    GL: {
        code: string;
        name: string;
    }[];
    GM: {
        code: string;
        name: string;
    }[];
    GN: {
        code: string;
        name: string;
    }[];
    GP: never[];
    GQ: {
        code: string;
        name: string;
    }[];
    GR: {
        code: string;
        name: string;
    }[];
    GS: never[];
    GT: {
        code: string;
        name: string;
    }[];
    GU: never[];
    GW: {
        code: string;
        name: string;
    }[];
    GY: {
        code: string;
        name: string;
    }[];
    HK: never[];
    HM: never[];
    HN: {
        code: string;
        name: string;
    }[];
    HR: {
        code: string;
        name: string;
    }[];
    HT: {
        code: string;
        name: string;
    }[];
    HU: {
        code: string;
        name: string;
    }[];
    ID: {
        code: string;
        name: string;
    }[];
    IE: {
        code: string;
        name: string;
    }[];
    IL: {
        code: string;
        name: string;
    }[];
    IM: never[];
    IN: {
        code: string;
        name: string;
    }[];
    IO: never[];
    IQ: {
        code: string;
        name: string;
    }[];
    IR: {
        code: string;
        name: string;
    }[];
    IS: {
        code: string;
        name: string;
    }[];
    IT: {
        code: string;
        name: string;
    }[];
    JE: never[];
    JM: {
        code: string;
        name: string;
    }[];
    JO: {
        code: string;
        name: string;
    }[];
    JP: {
        code: string;
        name: string;
    }[];
    KE: {
        code: string;
        name: string;
    }[];
    KG: {
        code: string;
        name: string;
    }[];
    KH: {
        code: string;
        name: string;
    }[];
    KI: {
        code: string;
        name: string;
    }[];
    KM: {
        code: string;
        name: string;
    }[];
    KN: {
        code: string;
        name: string;
    }[];
    KP: {
        code: string;
        name: string;
    }[];
    KR: {
        code: string;
        name: string;
    }[];
    KW: {
        code: string;
        name: string;
    }[];
    KY: never[];
    KZ: {
        code: string;
        name: string;
    }[];
    LA: {
        code: string;
        name: string;
    }[];
    LB: {
        code: string;
        name: string;
    }[];
    LC: {
        code: string;
        name: string;
    }[];
    LI: {
        code: string;
        name: string;
    }[];
    LK: {
        code: string;
        name: string;
    }[];
    LR: {
        code: string;
        name: string;
    }[];
    LS: {
        code: string;
        name: string;
    }[];
    LT: {
        code: string;
        name: string;
    }[];
    LU: {
        code: string;
        name: string;
    }[];
    LV: {
        code: string;
        name: string;
    }[];
    LY: {
        code: string;
        name: string;
    }[];
    MA: {
        code: string;
        name: string;
    }[];
    MC: {
        code: string;
        name: string;
    }[];
    MD: {
        code: string;
        name: string;
    }[];
    ME: {
        code: string;
        name: string;
    }[];
    MF: never[];
    MG: {
        code: string;
        name: string;
    }[];
    MH: {
        code: string;
        name: string;
    }[];
    MK: {
        code: string;
        name: string;
    }[];
    ML: {
        code: string;
        name: string;
    }[];
    MM: {
        code: string;
        name: string;
    }[];
    MN: {
        code: string;
        name: string;
    }[];
    MO: never[];
    MP: never[];
    MQ: never[];
    MR: {
        code: string;
        name: string;
    }[];
    MS: never[];
    MT: {
        code: string;
        name: string;
    }[];
    MU: {
        code: string;
        name: string;
    }[];
    MV: {
        code: string;
        name: string;
    }[];
    MW: {
        code: string;
        name: string;
    }[];
    MX: {
        code: string;
        name: string;
    }[];
    MY: {
        code: string;
        name: string;
    }[];
    MZ: {
        code: string;
        name: string;
    }[];
    NA: {
        code: string;
        name: string;
    }[];
    NC: never[];
    NE: {
        code: string;
        name: string;
    }[];
    NF: never[];
    NG: {
        code: string;
        name: string;
    }[];
    NI: {
        code: string;
        name: string;
    }[];
    NL: {
        code: string;
        name: string;
    }[];
    NO: {
        code: string;
        name: string;
    }[];
    NP: {
        code: string;
        name: string;
    }[];
    NR: {
        code: string;
        name: string;
    }[];
    NU: never[];
    NZ: {
        code: string;
        name: string;
    }[];
    OM: {
        code: string;
        name: string;
    }[];
    PA: {
        code: string;
        name: string;
    }[];
    PE: {
        code: string;
        name: string;
    }[];
    PF: never[];
    PG: {
        code: string;
        name: string;
    }[];
    PH: {
        code: string;
        name: string;
    }[];
    PK: {
        code: string;
        name: string;
    }[];
    PL: {
        code: string;
        name: string;
    }[];
    PM: never[];
    PN: never[];
    PR: never[];
    PS: {
        code: string;
        name: string;
    }[];
    PT: {
        code: string;
        name: string;
    }[];
    PW: {
        code: string;
        name: string;
    }[];
    PY: {
        code: string;
        name: string;
    }[];
    QA: {
        code: string;
        name: string;
    }[];
    RE: never[];
    RO: {
        code: string;
        name: string;
    }[];
    RS: {
        code: string;
        name: string;
    }[];
    RU: {
        code: string;
        name: string;
    }[];
    RW: {
        code: string;
        name: string;
    }[];
    SA: {
        code: string;
        name: string;
    }[];
    SB: {
        code: string;
        name: string;
    }[];
    SC: {
        code: string;
        name: string;
    }[];
    SD: {
        code: string;
        name: string;
    }[];
    SE: {
        code: string;
        name: string;
    }[];
    SG: {
        code: string;
        name: string;
    }[];
    SH: {
        code: string;
        name: string;
    }[];
    SI: {
        code: string;
        name: string;
    }[];
    SJ: never[];
    SK: {
        code: string;
        name: string;
    }[];
    SL: {
        code: string;
        name: string;
    }[];
    SM: {
        code: string;
        name: string;
    }[];
    SN: {
        code: string;
        name: string;
    }[];
    SO: {
        code: string;
        name: string;
    }[];
    SR: {
        code: string;
        name: string;
    }[];
    SS: {
        code: string;
        name: string;
    }[];
    ST: {
        code: string;
        name: string;
    }[];
    SV: {
        code: string;
        name: string;
    }[];
    SX: never[];
    SY: {
        code: string;
        name: string;
    }[];
    SZ: {
        code: string;
        name: string;
    }[];
    TC: never[];
    TD: {
        code: string;
        name: string;
    }[];
    TF: never[];
    TG: {
        code: string;
        name: string;
    }[];
    TH: {
        code: string;
        name: string;
    }[];
    TJ: {
        code: string;
        name: string;
    }[];
    TK: never[];
    TL: {
        code: string;
        name: string;
    }[];
    TM: {
        code: string;
        name: string;
    }[];
    TN: {
        code: string;
        name: string;
    }[];
    TO: {
        code: string;
        name: string;
    }[];
    TR: {
        code: string;
        name: string;
    }[];
    TT: {
        code: string;
        name: string;
    }[];
    TV: {
        code: string;
        name: string;
    }[];
    TW: {
        code: string;
        name: string;
    }[];
    TZ: {
        code: string;
        name: string;
    }[];
    UA: {
        code: string;
        name: string;
    }[];
    UG: {
        code: string;
        name: string;
    }[];
    UM: {
        code: string;
        name: string;
    }[];
    US: {
        code: string;
        name: string;
    }[];
    UY: {
        code: string;
        name: string;
    }[];
    UZ: {
        code: string;
        name: string;
    }[];
    VA: never[];
    VC: {
        code: string;
        name: string;
    }[];
    VE: {
        code: string;
        name: string;
    }[];
    VG: never[];
    VI: never[];
    VN: {
        code: string;
        name: string;
    }[];
    VU: {
        code: string;
        name: string;
    }[];
    WF: {
        code: string;
        name: string;
    }[];
    WS: {
        code: string;
        name: string;
    }[];
    YE: {
        code: string;
        name: string;
    }[];
    YT: never[];
    ZA: {
        code: string;
        name: string;
    }[];
    ZM: {
        code: string;
        name: string;
    }[];
    ZW: {
        code: string;
        name: string;
    }[];
};
export declare const SUBDIVISION_STATE_MAP: Record<SubdivisionCode, string>;
//# sourceMappingURL=country.db.d.ts.map