import { MediaType } from './mime-type.model';
import { MimeTypeDb, mimeTypeToMediaTypeDb } from './mime-type.db';
export function mimeTypeToExtension(mimeType) {
    return MimeTypeDb[mimeType];
}
export function mimeTypeToMediaType(mimeType) {
    var _a;
    return (_a = mimeTypeToMediaTypeDb[mimeType]) !== null && _a !== void 0 ? _a : MediaType.File;
}
//# sourceMappingURL=mime-type.js.map