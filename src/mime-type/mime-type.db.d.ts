import { Extension, MediaType, MimeType } from './mime-type.model';
export declare const mimeTypeToMediaTypeDb: Partial<Record<MimeType, MediaType>>;
export declare const MimeTypeDb: Record<MimeType, Extension[]>;
//# sourceMappingURL=mime-type.db.d.ts.map