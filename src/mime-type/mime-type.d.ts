import { Extension, MediaType, MimeType } from './mime-type.model';
export declare function mimeTypeToExtension(mimeType: MimeType): Extension[];
export declare function mimeTypeToMediaType(mimeType: string): MediaType;
//# sourceMappingURL=mime-type.d.ts.map