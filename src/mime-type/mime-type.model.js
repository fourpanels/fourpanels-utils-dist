export var MediaType;
(function (MediaType) {
    MediaType["Image"] = "IMAGE";
    MediaType["Pdf"] = "PDF";
    MediaType["Video"] = "VIDEO";
    MediaType["Audio"] = "AUDIO";
    MediaType["Dicom"] = "DICOM";
    MediaType["File"] = "FILE";
})(MediaType || (MediaType = {}));
//# sourceMappingURL=mime-type.model.js.map