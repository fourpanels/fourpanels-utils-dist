export declare function clone<T>(value: T): T;
export declare function deleteAllKeys<T>(object: T): void;
export declare function excludeKeys<T, U extends keyof T>(obj: T, exclude: U | U[]): Omit<T, U>;
//# sourceMappingURL=object.d.ts.map