import { toArray } from '../array/array';
export function clone(value) {
    return JSON.parse(JSON.stringify(value));
}
export function deleteAllKeys(object) {
    for (const key in object) {
        delete object[key];
    }
}
export function excludeKeys(obj, exclude) {
    const excludes = toArray(exclude);
    const result = {};
    for (const [key, value] of Object.entries(obj)) {
        if (!excludes.includes(key)) {
            // @ts-ignore
            result[key] = value;
        }
    }
    return result;
}
//# sourceMappingURL=object.js.map