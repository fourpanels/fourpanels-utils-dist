export function hasValue(value) {
    if (isNullish(value)) {
        return false;
    }
    if (isString(value)) {
        return value.trim() !== '';
    }
    if (isNumberOrNaN(value)) {
        return !isNaN(value);
    }
    if (isArray(value)) {
        return value.length > 0;
    }
    if (isObject(value)) {
        return Object.keys(value).length > 0;
    }
    return true;
}
export function hasNotValue(value) {
    return !hasValue(value);
}
// ------ Nullish ------
export function isNullish(value) {
    return value == null;
}
export function isNotNullish(value) {
    return !isNullish(value);
}
// ------ String ------
export function isStringAndNotEmpty(value) {
    return typeof value === 'string' && value !== '';
}
export function isStringAndNotBlank(value) {
    return typeof value === 'string' && value.trim() !== '';
}
export function isString(value) {
    return typeof value === 'string';
}
export function isNotString(value) {
    return !isString(value);
}
// ------ Number ------
export function isNumberOrNaN(value) {
    return typeof value === 'number';
}
export function isNumber(value) {
    return typeof value === 'number' && !isNaN(value);
}
export function isNotNumber(value) {
    return !isNumber(value);
}
// ------ Boolean ------
export function isBoolean(value) {
    return typeof value === 'boolean';
}
export function isNotBoolean(value) {
    return !isBoolean(value);
}
// ------ Array ------
export function isArray(value) {
    return Array.isArray(value);
}
export function isNotArray(value) {
    return !isArray(value);
}
// ------ Blob ------
export function isBlob(value) {
    return (value instanceof Blob) && typeof value === 'object';
}
export function isNotBlob(value) {
    return !isBlob(value);
}
// ------ Object ------
export function isObject(value) {
    return typeof value === 'object';
}
export function isNutObject(value) {
    return !isObject(value);
}
// ------ Equality ------
export function isAbstractEqual(a, b) {
    return a == b;
}
export function isStrictEqual(a, b) {
    return a === b;
}
//# sourceMappingURL=ensure.js.map