import { LANGUAGES_MAP } from './language.db';
export function getLanguageFromCode(languageCode) {
    return LANGUAGES_MAP[languageCode];
}
//# sourceMappingURL=language.js.map