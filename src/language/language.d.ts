import { Language, LanguageCode } from './language.model';
export declare function getLanguageFromCode(languageCode: LanguageCode): Language;
//# sourceMappingURL=language.d.ts.map