import { TimeStamp } from './date.model';
import { Moment } from 'moment';
export declare const getDateFromTimestamp: (t: TimeStamp) => number;
export declare const isSameDate: (t1: TimeStamp | Moment | Date, t2: TimeStamp | Moment | Date) => boolean;
//# sourceMappingURL=date.d.ts.map