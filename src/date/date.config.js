import { DurationUnitFormat } from './date.model';
/**
 * Note: Local file
 *
 * This file is just for inside of project.
 * We will not export it in index.ts
 */
export const DurationUnitFormatValue = {
    [DurationUnitFormat.Short]: {
        Plural: {
            minutes: $localize `m`,
            hours: $localize `h`,
            days: $localize `d`
        },
        Singular: {
            minute: $localize `m`,
            hour: $localize `h`,
            day: $localize `d`
        },
    },
    [DurationUnitFormat.Full]: {
        Plural: {
            minutes: $localize `Minutes`,
            hours: $localize `Hours`,
            days: $localize `Days`
        },
        Singular: {
            minute: $localize `Minute`,
            hour: $localize `Hour`,
            day: $localize `Day`
        },
    },
};
//# sourceMappingURL=date.config.js.map