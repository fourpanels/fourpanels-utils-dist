import { DurationUnitFormat, DurationUnitFormatText } from './date.model';
/**
 * Note: Local file
 *
 * This file is just for inside of project.
 * We will not export it in index.ts
 */
export declare const DurationUnitFormatValue: {
    short: DurationUnitFormatText;
    full: DurationUnitFormatText;
};
//# sourceMappingURL=date.config.d.ts.map