import moment from 'moment';
import { DurationFormat, DurationUnitFormat } from './date.model';
export declare function getDurationFromNowUntil(endTimestamp: number): moment.Duration | null;
/**
 * @param endTimestamp
 * @param format
 * @param unitFormat
 * @param defaultValueOnNegativeDuration return `defaultValueOnNegativeDuration` value if duration is negative
 */
export declare function getDurationTextFromNowUntil(endTimestamp: number, format?: DurationFormat, unitFormat?: DurationUnitFormat, defaultValueOnNegativeDuration?: string | null): string;
/**
 * @param duration
 * @param unitFormat
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d' | '12h' | '25m'
 *
 */
export declare function durationToShortFormatText(duration: moment.Duration, unitFormat?: DurationUnitFormat, defaultValueOnNegativeDuration?: string | null): string;
/**
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d 10h' | '12h 56m' | '25m'
 *
 */
export declare function durationToMediumFormatText(duration: moment.Duration, unitFormat?: DurationUnitFormat, defaultValueOnNegativeDuration?: string | null): string;
/**
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d 10h 40m' | '12h 56m' | '25m'
 *
 */
export declare function durationToLangFormatText(duration: moment.Duration, unitFormat?: DurationUnitFormat, defaultValueOnNegativeDuration?: string | null): string;
//# sourceMappingURL=duration.d.ts.map