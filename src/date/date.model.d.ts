export declare type TimeStamp = number;
export declare enum DurationFormat {
    Short = "short",
    Medium = "medium",
    Lang = "lang"
}
export declare enum DurationUnitFormat {
    Short = "short",
    Full = "full"
}
export interface DurationUnitFormatText {
    Plural: {
        minutes: string;
        hours: string;
        days: string;
    };
    Singular: {
        minute: string;
        hour: string;
        day: string;
    };
}
//# sourceMappingURL=date.model.d.ts.map