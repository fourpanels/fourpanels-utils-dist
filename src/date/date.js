import { isNullish } from '../ensure/ensure';
import moment from 'moment';
export const getDateFromTimestamp = (t) => moment(t).startOf('day').valueOf();
export const isSameDate = (t1, t2) => {
    if (isNullish(t1) || isNullish(t2))
        return false;
    return moment(t1).isSame(moment(t2), 'days');
};
//# sourceMappingURL=date.js.map