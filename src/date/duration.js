import moment from 'moment';
import { isNotNullish } from '../ensure/ensure';
import { DurationUnitFormatValue } from './date.config';
import { DurationFormat, DurationUnitFormat } from './date.model';
export function getDurationFromNowUntil(endTimestamp) {
    if (!endTimestamp) {
        return null;
    }
    const dateEnd = moment(endTimestamp);
    const dateNow = moment();
    const duration = moment.duration(dateEnd.diff(dateNow));
    return duration;
}
/**
 * @param endTimestamp
 * @param format
 * @param unitFormat
 * @param defaultValueOnNegativeDuration return `defaultValueOnNegativeDuration` value if duration is negative
 */
export function getDurationTextFromNowUntil(endTimestamp, format = DurationFormat.Medium, unitFormat = DurationUnitFormat.Short, defaultValueOnNegativeDuration = null) {
    const duration = getDurationFromNowUntil(endTimestamp);
    if (!duration) {
        return '';
    }
    switch (format) {
        case DurationFormat.Short:
            return durationToShortFormatText(duration, unitFormat, defaultValueOnNegativeDuration);
        case DurationFormat.Medium:
            return durationToMediumFormatText(duration, unitFormat, defaultValueOnNegativeDuration);
        case DurationFormat.Lang:
            return durationToLangFormatText(duration, unitFormat, defaultValueOnNegativeDuration);
    }
}
/**
 * @param duration
 * @param unitFormat
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d' | '12h' | '25m'
 *
 */
export function durationToShortFormatText(duration, unitFormat = DurationUnitFormat.Short, defaultValueOnNegativeDuration = null) {
    if (duration.asSeconds() < 1 && isNotNullish(defaultValueOnNegativeDuration)) {
        return defaultValueOnNegativeDuration;
    }
    const unitFormatValue = DurationUnitFormatValue[unitFormat];
    // DAY
    const oneDayDuration = moment.duration(1, 'days');
    const ceilHourDuration = getMomentCeil(duration, 'hours');
    if (ceilHourDuration.asSeconds() >= oneDayDuration.asSeconds()) {
        return getUnitFormattedText({
            days: ceilHourDuration.days(),
        }, unitFormatValue, unitFormat);
    }
    // HOUR
    const oneHourDuration = moment.duration(1, 'hours');
    const ceilHoursDuration = getMomentCeil(duration, 'hours');
    if (duration.asSeconds() >= oneHourDuration.asSeconds()) {
        return getUnitFormattedText({
            hours: ceilHoursDuration.hours(),
        }, unitFormatValue, unitFormat);
    }
    // MINUTE
    const ceilMinutesDuration = getMomentCeil(duration, 'minutes');
    return getUnitFormattedText({
        minutes: ceilMinutesDuration.minutes(),
    }, unitFormatValue, unitFormat);
}
/**
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d 10h' | '12h 56m' | '25m'
 *
 */
export function durationToMediumFormatText(duration, unitFormat = DurationUnitFormat.Short, defaultValueOnNegativeDuration = null) {
    if (duration.asSeconds() < 1 && isNotNullish(defaultValueOnNegativeDuration)) {
        return defaultValueOnNegativeDuration;
    }
    const unitFormatValue = DurationUnitFormatValue[unitFormat];
    // DAYS & HOURS
    const oneDayDuration = moment.duration(1, 'days');
    const ceilHourDuration = getMomentCeil(duration, 'hours');
    if (ceilHourDuration.asSeconds() >= oneDayDuration.asSeconds()) {
        return getUnitFormattedText({
            days: ceilHourDuration.days(),
            hours: ceilHourDuration.hours()
        }, unitFormatValue, unitFormat);
    }
    // HOURS & MINUTES
    const oneHourDuration = moment.duration(1, 'hours');
    const ceilMinuteDuration = getMomentCeil(duration, 'minutes');
    if (ceilMinuteDuration.asSeconds() >= oneHourDuration.asSeconds()) {
        return getUnitFormattedText({
            hours: ceilMinuteDuration.hours(),
            minutes: ceilMinuteDuration.minutes()
        }, unitFormatValue, unitFormat);
    }
    // MINUTES
    const ceilDuration = getMomentCeil(duration, 'minutes');
    return getUnitFormattedText({
        minutes: ceilDuration.minutes()
    }, unitFormatValue, unitFormat);
}
/**
 * @param defaultValueOnNegativeDuration return this value if duration is negative
 *
 * @returns '23d 10h 40m' | '12h 56m' | '25m'
 *
 */
export function durationToLangFormatText(duration, unitFormat = DurationUnitFormat.Short, defaultValueOnNegativeDuration = null) {
    if (duration.asSeconds() < 1 && isNotNullish(defaultValueOnNegativeDuration)) {
        return defaultValueOnNegativeDuration;
    }
    const unitFormatValue = DurationUnitFormatValue[unitFormat];
    // DAYS & HOURS & MINUTE
    const oneDayDuration = moment.duration(1, 'days');
    const ceilHourDuration = getMomentCeil(duration, 'hours');
    if (ceilHourDuration.asSeconds() >= oneDayDuration.asSeconds()) {
        return getUnitFormattedText({
            days: ceilHourDuration.days(),
            hours: ceilHourDuration.hours(),
            minutes: ceilHourDuration.minutes(),
        }, unitFormatValue, unitFormat);
    }
    // HOURS & MINUTES
    const oneHourDuration = moment.duration(1, 'hours');
    const ceilMinuteDuration = getMomentCeil(duration, 'minutes');
    if (ceilMinuteDuration.asSeconds() >= oneHourDuration.asSeconds()) {
        return getUnitFormattedText({
            hours: ceilMinuteDuration.hours(),
            minutes: ceilMinuteDuration.minutes()
        }, unitFormatValue, unitFormat);
    }
    // MINUTES
    const ceilDuration = getMomentCeil(duration, 'minutes');
    return getUnitFormattedText({
        minutes: ceilDuration.minutes()
    }, unitFormatValue, unitFormat);
}
/* ---------------------------
 *           Helpers
 * ----------------------------
 */
function getMomentCeil(duration, key = 'minutes') {
    const cloneDuration = duration.clone();
    switch (key) {
        case 'hours':
            const minute = cloneDuration.minutes();
            const diffMinute = minute === 0 ? 0 : 60 - minute;
            return cloneDuration.add(diffMinute, 'minutes');
        case 'minutes':
            const seconds = cloneDuration.seconds();
            const diffSecond = seconds === 0 ? 0 : 60 - seconds;
            return cloneDuration.add(diffSecond, 'seconds');
    }
}
function getUnitFormattedText({ days, hours, minutes }, unitFormatValue, unitFormat) {
    let result = '';
    if (isNotNullish(days)) {
        result += `${days}${unitFormat === DurationUnitFormat.Full ? ' ' : ''}${days === 1 ? unitFormatValue.Singular.day : unitFormatValue.Plural.days} `;
    }
    if (isNotNullish(hours)) {
        result += `${hours}${unitFormat === DurationUnitFormat.Full ? ' ' : ''}${hours === 1 ? unitFormatValue.Singular.hour : unitFormatValue.Plural.hours} `;
    }
    if (isNotNullish(minutes)) {
        result += `${minutes}${unitFormat === DurationUnitFormat.Full ? ' ' : ''}${minutes === 1 ? unitFormatValue.Singular.minute : unitFormatValue.Plural.minutes} `;
    }
    return result.trim();
}
//# sourceMappingURL=duration.js.map