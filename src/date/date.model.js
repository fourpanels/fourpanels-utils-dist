export var DurationFormat;
(function (DurationFormat) {
    DurationFormat["Short"] = "short";
    DurationFormat["Medium"] = "medium";
    DurationFormat["Lang"] = "lang";
})(DurationFormat || (DurationFormat = {}));
export var DurationUnitFormat;
(function (DurationUnitFormat) {
    DurationUnitFormat["Short"] = "short";
    DurationUnitFormat["Full"] = "full";
})(DurationUnitFormat || (DurationUnitFormat = {}));
//# sourceMappingURL=date.model.js.map