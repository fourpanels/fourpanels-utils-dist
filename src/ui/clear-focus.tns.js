import { isIOS, Frame } from "@nativescript/core";
/**
 * When we should send reference of inputElement to this function?
 *
 * If textField element is a child of native modal, then we have to send textField reference.
 * Otherwise application will be crash or noting will be happened (depend on platform).
 *
 * But if textField is a child of main Frame we don't need send any reference. Nativescript can hide keyboard without
 * reference of textField
 *
 */
export function clearFocus(view) {
    if (view) {
        view.dismissSoftInput();
        return;
    }
    /**
     * Other solution to clearFocus:
     *
     * 1- this.inputElement?.dismissSoftInput();
     *    Android: we need need reference of active input element.
     *             keyboard will be close but focus will be stay on element.
     *    Ios: Work fine.
     *
     * 2- this.activeInput?.android.clearFocus();
     *    Android: work fine but We need need then reference of active input element.
     *    Ios: This method is just for android
     *
     * 3- ad.getInputMethodManager().dismissSoftInput();
     *    Android: keyboard will be close but focus stay on element.
     *    Ios: Work fine
     */
    if (isIOS) {
        Frame.topmost().nativeView.endEditing(true);
    }
    else {
        Frame.topmost().nativeView.clearFocus();
    }
}
//# sourceMappingURL=clear-focus.tns.js.map