import { isAndroid } from '@nativescript/core';
import { android as appAndroid, ios as appIos } from '@nativescript/core/application';
export function getNsStatusBarHeight() {
    if (isAndroid) {
        const resourceId = appAndroid.context.getResources().getIdentifier('status_bar_height', 'dimen', 'android');
        const result = appAndroid.context.getResources().getDimensionPixelSize(resourceId);
        return result / appAndroid.context.getResources().getDisplayMetrics().density;
        // IOS
    }
    else {
        return appIos.nativeApp.statusBarFrame.size.height;
    }
}
//# sourceMappingURL=ns-status-bar.tns.js.map