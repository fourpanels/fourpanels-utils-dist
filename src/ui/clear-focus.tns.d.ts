import { TextField, TextView } from "@nativescript/core";
/**
 * When we should send reference of inputElement to this function?
 *
 * If textField element is a child of native modal, then we have to send textField reference.
 * Otherwise application will be crash or noting will be happened (depend on platform).
 *
 * But if textField is a child of main Frame we don't need send any reference. Nativescript can hide keyboard without
 * reference of textField
 *
 */
export declare function clearFocus(view?: TextField | TextView): void;
//# sourceMappingURL=clear-focus.tns.d.ts.map