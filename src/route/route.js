export function toRoutePath(...paths) {
    return ['/' + paths.filter(path => path.trim() !== '').join('/')];
}
//# sourceMappingURL=route.js.map