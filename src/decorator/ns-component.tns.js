import { registerElement } from '@nativescript/angular';
import { AbsoluteLayout, ContentView, DockLayout, FlexboxLayout, GridLayout, isAndroid, ProxyViewContainer, ScrollView, StackLayout, WrapLayout } from '@nativescript/core';
// -----  MODEL -----
export var NsComponentHostLayout;
(function (NsComponentHostLayout) {
    NsComponentHostLayout["AbsoluteLayout"] = "AbsoluteLayout";
    NsComponentHostLayout["DockLayout"] = "DockLayout";
    NsComponentHostLayout["GridLayout"] = "GridLayout";
    NsComponentHostLayout["WrapLayout"] = "WrapLayout";
    NsComponentHostLayout["StackLayout"] = "StackLayout";
    NsComponentHostLayout["FlexboxLayout"] = "FlexboxLayout";
    NsComponentHostLayout["ContentView"] = "ContentView";
    NsComponentHostLayout["ScrollView"] = "ScrollView";
    NsComponentHostLayout["ProxyViewContainer"] = "ProxyViewContainer";
    NsComponentHostLayout["AbsoluteLayoutOverFLow"] = "AbsoluteLayoutOverFlow";
    NsComponentHostLayout["StackLayoutOverFLow"] = "StackLayoutOverFlow";
    NsComponentHostLayout["GridLayoutOverFLow"] = "GridLayoutOverFlow";
    NsComponentHostLayout["FlexboxLayoutOverFLow"] = "FlexboxLayoutOverFlow";
    NsComponentHostLayout["ContentViewOverFLow"] = "ContentViewOverFlow";
})(NsComponentHostLayout || (NsComponentHostLayout = {}));
// -----  Utils -----
export function setLayoutOverflowToVisible(layout) {
    const overflowVisible = () => {
        if (isAndroid) {
            layout.android.getParent().setClipChildren(false);
        }
        else {
            layout.clipToBounds = false;
        }
    };
    if (layout.isLoaded) {
        overflowVisible();
    }
    else {
        layout.once('loaded', overflowVisible);
    }
}
// -----  Class Overflow -----
class AbsoluteLayoutOverFLow extends AbsoluteLayout {
    constructor() {
        super();
        this.once('loaded', e => setLayoutOverflowToVisible(e.object));
    }
}
class StackLayoutOverFLow extends StackLayout {
    constructor() {
        super();
        this.once('loaded', e => setLayoutOverflowToVisible(e.object));
    }
}
class GridLayoutOverFLow extends GridLayout {
    constructor() {
        super();
        this.once('loaded', e => setLayoutOverflowToVisible(e.object));
    }
}
class FlexboxLayoutOverFLow extends FlexboxLayout {
    constructor() {
        super();
        this.once('loaded', e => setLayoutOverflowToVisible(e.object));
    }
}
class ContentViewOverFLow extends ContentView {
    constructor() {
        super();
        this.once('loaded', e => setLayoutOverflowToVisible(e.object));
    }
}
const createClass = (className, extendsClassName) => ({ [className]: class extends extendsClassName {
    } })[className];
// Problem:
//   Nativescript uses for all it's components the `ProxyViewContainer` class which is not styleable.
// Solution:
//   We register all Nativescript components with the `registerElement` function from Nativescript and passing our custom layout.
//   This also improves debugging in the browser dev tools to see the the 'real' class names of the components.
// -----  REGISTER LAYOUT -----
function registerComponentAsLayout(selector, hostLayout) {
    switch (hostLayout) {
        case NsComponentHostLayout.AbsoluteLayout:
            const classRefAbsoluteLayout = createClass(selector, AbsoluteLayout);
            registerElement(selector, () => classRefAbsoluteLayout);
            return;
        case NsComponentHostLayout.DockLayout:
            const classRefDockLayout = createClass(selector, DockLayout);
            registerElement(selector, () => classRefDockLayout);
            return;
        case NsComponentHostLayout.GridLayout:
            const classRefGridLayout = createClass(selector, GridLayout);
            registerElement(selector, () => classRefGridLayout);
            return;
        case NsComponentHostLayout.WrapLayout:
            const classRefWrapLayout = createClass(selector, WrapLayout);
            registerElement(selector, () => classRefWrapLayout);
            return;
        case NsComponentHostLayout.StackLayout:
            const classRefStackLayout = createClass(selector, StackLayout);
            registerElement(selector, () => classRefStackLayout);
            return;
        case NsComponentHostLayout.FlexboxLayout:
            const classRefFlexboxLayout = createClass(selector, FlexboxLayout);
            registerElement(selector, () => classRefFlexboxLayout);
            return;
        case NsComponentHostLayout.ContentView:
            const classRefContentView = createClass(selector, ContentView);
            registerElement(selector, () => classRefContentView);
            return;
        case NsComponentHostLayout.ScrollView:
            const classRefScrollView = createClass(selector, ScrollView);
            registerElement(selector, () => classRefScrollView);
            return;
        case NsComponentHostLayout.ProxyViewContainer:
            const classRefProxyViewContainer = createClass(selector, ProxyViewContainer);
            registerElement(selector, () => classRefProxyViewContainer);
            return;
        // -----  Overflow -----
        case NsComponentHostLayout.AbsoluteLayoutOverFLow:
            registerElement(selector, () => AbsoluteLayoutOverFLow);
            return;
        case NsComponentHostLayout.StackLayoutOverFLow:
            registerElement(selector, () => StackLayoutOverFLow);
            return;
        case NsComponentHostLayout.GridLayoutOverFLow:
            registerElement(selector, () => GridLayoutOverFLow);
            return;
        case NsComponentHostLayout.FlexboxLayoutOverFLow:
            registerElement(selector, () => FlexboxLayoutOverFLow);
            return;
        case NsComponentHostLayout.ContentViewOverFLow:
            registerElement(selector, () => ContentViewOverFLow);
            return;
    }
}
// -----  DECORATOR -----
export function NsComponent(opt) {
    if (opt.hostLayout && opt.selector) {
        registerComponentAsLayout(opt.selector, opt.hostLayout);
    }
    return () => { };
}
//# sourceMappingURL=ns-component.tns.js.map