/**
 * Error Handling
 */
export interface PropConfig {
    /**
     * A list of method names on this class
     *
     * Type of method in class: ( (value?: T) => boolean )[]
     */
    setIf?: string[];
    /**
     * A method name on this class
     *
     * Type of method in class: ( (value: T, changes?: PropSetterChange) => T )
     */
    setter?: string;
    /**
     * A list of method names on this class
     *
     * Type of method in class: ( (value?: T, changes?: PropChange<T>) => void )[]
     */
    onChange?: string[];
    /**
     * The method name on this class
     *
     * Type of method in class: () => T
     */
    getter?: string;
}
export interface PropChange<T> {
    isFirstChange: boolean;
    previousValue: T;
    currentValue: T;
}
export interface PropSetterChange {
    isFirstChange: boolean;
}
export declare function Prop<T>(config: PropConfig): (target: Object, propertyKey: string) => void;
//# sourceMappingURL=prop.d.ts.map