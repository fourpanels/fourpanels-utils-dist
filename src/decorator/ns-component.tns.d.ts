import { LayoutBase } from '@nativescript/core';
export declare enum NsComponentHostLayout {
    AbsoluteLayout = "AbsoluteLayout",
    DockLayout = "DockLayout",
    GridLayout = "GridLayout",
    WrapLayout = "WrapLayout",
    StackLayout = "StackLayout",
    FlexboxLayout = "FlexboxLayout",
    ContentView = "ContentView",
    ScrollView = "ScrollView",
    ProxyViewContainer = "ProxyViewContainer",
    AbsoluteLayoutOverFLow = "AbsoluteLayoutOverFlow",
    StackLayoutOverFLow = "StackLayoutOverFlow",
    GridLayoutOverFLow = "GridLayoutOverFlow",
    FlexboxLayoutOverFLow = "FlexboxLayoutOverFlow",
    ContentViewOverFLow = "ContentViewOverFlow"
}
export interface NsComponent {
    hostLayout?: NsComponentHostLayout;
    selector?: string;
}
export declare function setLayoutOverflowToVisible(layout: LayoutBase): void;
export declare function NsComponent(opt: NsComponent): ClassDecorator;
//# sourceMappingURL=ns-component.tns.d.ts.map