// -----  MODEL -----
export var NsComponentHostLayout;
(function (NsComponentHostLayout) {
    NsComponentHostLayout["AbsoluteLayout"] = "AbsoluteLayout";
    NsComponentHostLayout["DockLayout"] = "DockLayout";
    NsComponentHostLayout["GridLayout"] = "GridLayout";
    NsComponentHostLayout["WrapLayout"] = "WrapLayout";
    NsComponentHostLayout["StackLayout"] = "StackLayout";
    NsComponentHostLayout["FlexboxLayout"] = "FlexboxLayout";
    NsComponentHostLayout["ContentView"] = "ContentView";
    NsComponentHostLayout["ScrollView"] = "ScrollView";
    NsComponentHostLayout["ProxyViewContainer"] = "ProxyViewContainer";
    NsComponentHostLayout["AbsoluteLayoutOverFLow"] = "AbsoluteLayoutOverFlow";
    NsComponentHostLayout["StackLayoutOverFLow"] = "StackLayoutOverFlow";
    NsComponentHostLayout["GridLayoutOverFLow"] = "GridLayoutOverFlow";
    NsComponentHostLayout["FlexboxLayoutOverFLow"] = "FlexboxLayoutOverFlow";
    NsComponentHostLayout["ContentViewOverFLow"] = "ContentViewOverFlow";
})(NsComponentHostLayout || (NsComponentHostLayout = {}));
// -----  Utils -----
export function setLayoutOverflowToVisible(layout) {
}
// -----  DECORATOR -----
export function NsComponent(opt) {
    return () => { };
}
//# sourceMappingURL=ns-component.js.map