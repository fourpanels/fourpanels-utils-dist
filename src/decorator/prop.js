/**
 * Error Handling
 */
export function Prop(config) {
    return function (target, propertyKey) {
        let _value;
        let _previousValue;
        let _isFirstChange = true;
        const getter = function () {
            // getter
            return config.getter ?
                target[config.getter].call(this, _value) :
                _value;
        };
        const setter = function (newVal) {
            // SetIf
            var _a;
            const canSetValue = config.setIf ?
                config.setIf.every(functionName => target[functionName].call(this, newVal)) :
                true;
            if (!canSetValue) {
                return;
            }
            // Setter
            _previousValue = _value;
            const setterChanges = {
                isFirstChange: _isFirstChange
            };
            _value = config.setter ?
                target[config.setter].call(this, newVal, setterChanges) :
                newVal;
            // onChange
            const changes = {
                isFirstChange: _isFirstChange,
                previousValue: _previousValue,
                currentValue: _value,
            };
            (_a = config.onChange) === null || _a === void 0 ? void 0 : _a.forEach(functionName => target[functionName].call(this, _value, changes));
            _isFirstChange = false;
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
        });
    };
}
//# sourceMappingURL=prop.js.map