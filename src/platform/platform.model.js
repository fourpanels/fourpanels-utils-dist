export var PlatformSpeedMode;
(function (PlatformSpeedMode) {
    PlatformSpeedMode[PlatformSpeedMode["Slow"] = 0] = "Slow";
    PlatformSpeedMode[PlatformSpeedMode["Normal"] = 1] = "Normal";
    PlatformSpeedMode[PlatformSpeedMode["Fast"] = 2] = "Fast";
})(PlatformSpeedMode || (PlatformSpeedMode = {}));
//# sourceMappingURL=platform.model.js.map