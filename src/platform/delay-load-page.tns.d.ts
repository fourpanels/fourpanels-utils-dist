/**
 * @returns delay time in milliseconds
 */
export declare function getDelayTimeToLoadPage(): number;
//# sourceMappingURL=delay-load-page.tns.d.ts.map