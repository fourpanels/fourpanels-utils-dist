export declare enum PlatformSpeedMode {
    Slow = 0,
    Normal = 1,
    Fast = 2
}
//# sourceMappingURL=platform.model.d.ts.map