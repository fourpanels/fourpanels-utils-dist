import { Device, isIOS } from '@nativescript/core/platform';
import { PlatformSpeedMode } from './platform.model';
/**
 * Find Os Versions:
 *    Ios:     https://en.wikipedia.org/wiki/IOS_version_history
 *    Android: https://en.wikipedia.org/wiki/Android_version_history
 */
const IOS_MIN_NORMAL_PLATFORM_VERSION = 10; // Since June 13, 2016
const IOS_MAX_NORMAL_PLATFORM_VERSION = 12; // Since June 4, 2018
const ANDROID_MIN_NORMAL_PLATFORM_VERSION = 7; // Since August 22, 2016
const ANDROID_MAX_NORMAL_PLATFORM_VERSION = 9; // Since August 6, 2018
export function getPlatformSpeedMode() {
    // Original versions has two or tree parts (i.e. Android: 9.0.1, Ios: 12.2). But we need only first part.
    const osVersion = parseInt(Device.osVersion.split('.')[0]);
    const minNormalVersion = isIOS ? IOS_MIN_NORMAL_PLATFORM_VERSION : ANDROID_MIN_NORMAL_PLATFORM_VERSION;
    const maxNormalVersion = isIOS ? IOS_MAX_NORMAL_PLATFORM_VERSION : ANDROID_MAX_NORMAL_PLATFORM_VERSION;
    if (osVersion < minNormalVersion) {
        return PlatformSpeedMode.Slow;
    }
    if (osVersion > maxNormalVersion) {
        return PlatformSpeedMode.Fast;
    }
    return PlatformSpeedMode.Normal;
}
//# sourceMappingURL=platform-speed.tns.js.map