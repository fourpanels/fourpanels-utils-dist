import { getPlatformSpeedMode } from "./platform-speed";
import { PlatformSpeedMode } from "./platform.model";
// Values are iin milliseconds
const DELAY_TIME_LOADING_PAGE = {
    [PlatformSpeedMode.Slow]: 500,
    [PlatformSpeedMode.Normal]: 150,
    [PlatformSpeedMode.Fast]: 30,
};
/**
 * @returns delay time in milliseconds
 */
export function getDelayTimeToLoadPage() {
    const platformSpeedMode = getPlatformSpeedMode();
    return DELAY_TIME_LOADING_PAGE[platformSpeedMode];
}
//# sourceMappingURL=delay-load-page.tns.js.map