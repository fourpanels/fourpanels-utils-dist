import { Observable } from "rxjs";
import { share } from "rxjs/operators";
export function intersectionObserver$(elem, config) {
    return new Observable(subscriber => {
        const callBack = (entries, observer) => subscriber.next({ entries, observer });
        const onSubscribe = () => observer.disconnect();
        const observer = new IntersectionObserver(callBack, config);
        observer.observe(elem);
        return onSubscribe;
    }).pipe(share());
}
//# sourceMappingURL=intersection-observer.js.map