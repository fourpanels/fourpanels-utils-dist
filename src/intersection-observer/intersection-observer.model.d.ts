export interface IntersectionObserverConfig extends IntersectionObserverInit {
}
export interface IntersectionObserverChanges {
    entries: IntersectionObserverEntry[];
    observer: IntersectionObserver;
}
//# sourceMappingURL=intersection-observer.model.d.ts.map