import { Observable } from "rxjs";
import { IntersectionObserverChanges, IntersectionObserverConfig } from "./intersection-observer.model";
export declare function intersectionObserver$(elem: HTMLElement, config?: IntersectionObserverConfig): Observable<IntersectionObserverChanges>;
//# sourceMappingURL=intersection-observer.d.ts.map