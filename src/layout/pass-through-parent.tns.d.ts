/**
 * 1- When can we use this function:
 * In IOS isPassThroughParentEnabled is by default false and behind element will not receive events (i.e. pan event)
 *
 * for example:
 *  <GridLayout>
 *    <StackLayout id="behind" width="100%" height="100%" (tap)="onTap()"></StackLayout>
 *    <StackLayout id="front" width="100%" height="100%" (tap)="onTap()"></StackLayout>
 *  </GridLayout>
 *
 * In this example, tap the event on a front element will be fired, and behind will never receive the event.
 * With isPassThroughParentEnabled=true ios will send an event to the parent or behind elements.
 *
 * This problem is just in IOS. In android, everything works fine.
 * In IOS, isPassThroughParentEnabled is by default false, and to override it,
 * we have to go all children elements and set isPassThroughParentEnabled to true.
 *
 *
 * 2- Element type is any:
 * There is many type for element (i.e. LayoutBase, ViewBase, ViewGroup and ...)
 * before each action we have to check is the action is exist for this element.
 *
 */
export declare function iosSetIsPassThroughParentEnabled(element: any): void;
//# sourceMappingURL=pass-through-parent.tns.d.ts.map