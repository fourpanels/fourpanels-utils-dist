import { Predicate } from './array.model';
export declare const toArray: (value: any | any[]) => any[];
export declare const filtersBy: <T>(filters: Predicate<T>[]) => (value: T) => boolean;
export declare const pushIf: <T>(value: T | T[], condition: boolean) => T[];
export declare const isArraySame: <T>(a: any, b: any) => boolean;
declare type SequenceMapper<T> = (seqValue: number, idx: number) => T;
/**
 * Generates a sequence of numbers from `start` (inclusive) to `end` (inclusive) moving `step` forward.
 */
export declare const sequenceOf: (start: number, end: number, step?: number) => number[];
/**
 * Generates a sequence of elements by a `mapper`. The mapper gets current index and sequence value passed.
 */
export declare const sequenceMappedOf: <T>(start: number, end: number, mapper: SequenceMapper<T>, step?: number) => T[];
export {};
//# sourceMappingURL=array.d.ts.map