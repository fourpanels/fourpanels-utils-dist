import { isArray } from '../ensure/ensure';
export const toArray = (value) => Array.isArray(value) ? value : [value];
export const filtersBy = (filters) => (value) => filters.every(filter => filter(value));
export const pushIf = (value, condition) => condition ? toArray(value) : [];
export const isArraySame = (a, b) => {
    if (isArray(a) && isArray(b)) {
        return (a.length === b.length &&
            a.every((fromA, index) => fromA === b[index]));
    }
    return false;
};
/**
 * Generates a sequence of numbers from `start` (inclusive) to `end` (inclusive) moving `step` forward.
 */
export const sequenceOf = (start, end, step = 1) => Array.from({ length: (end - start) / step + 1 }, (_, idx) => start + (idx * step));
/**
 * Generates a sequence of elements by a `mapper`. The mapper gets current index and sequence value passed.
 */
export const sequenceMappedOf = (start, end, mapper, step = 1) => Array.from({ length: (end - start) / step + 1 }, (_, idx) => mapper(start + (idx * step), idx));
//# sourceMappingURL=array.js.map