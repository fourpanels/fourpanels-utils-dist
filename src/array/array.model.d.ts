export declare type Predicate<T> = (t: T) => boolean;
export declare type Comparator<T> = (t1: T, t2: T) => number;
//# sourceMappingURL=array.model.d.ts.map