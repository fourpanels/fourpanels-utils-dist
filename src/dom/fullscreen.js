export function requestFullscreen(elem) {
    /**
     * Standard Browsers
     */
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
        return;
    }
    /**
     * Safari
     */
    if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
        return;
    }
    /**
     * IE11
     */
    if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    }
}
export function exitFullscreen() {
    /**
     * Standard Browsers
     */
    if (document.exitFullscreen) {
        document.exitFullscreen();
        return;
    }
    /**
     * Safari
     */
    if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
        return;
    }
    /**
     * IE11
     */
    if (document.msExitFullscreen) {
        document.msExitFullscreen();
        return;
    }
}
export function getFullscreenElement() {
    var _a, _b;
    return ((_b = (_a = document.fullscreenElement) !== null && _a !== void 0 ? _a : document.webkitFullscreenElement) !== null && _b !== void 0 ? _b : document.msFullscreenElement // IE11
    );
}
//# sourceMappingURL=fullscreen.js.map