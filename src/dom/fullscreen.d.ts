export declare function requestFullscreen(elem: HTMLElement): void;
export declare function exitFullscreen(): void;
export declare function getFullscreenElement(): any;
//# sourceMappingURL=fullscreen.d.ts.map