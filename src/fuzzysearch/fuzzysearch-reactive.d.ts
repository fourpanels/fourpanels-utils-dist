import { Observable } from 'rxjs';
interface FormControl {
    valueChanges: Observable<string>;
    valid: boolean;
}
export declare function reactiveFormFuzzysearch<T>(formCtrl: FormControl, options: T[], getSearchField?: (value: T) => string, { debounce }?: {
    debounce?: number;
}): Observable<T[]>;
export {};
//# sourceMappingURL=fuzzysearch-reactive.d.ts.map