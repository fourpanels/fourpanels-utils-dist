import { debounceTime, filter, map, startWith, share } from 'rxjs/operators';
import { fuzzysearch } from './fuzzysearch';
export function reactiveFormFuzzysearch(formCtrl, options, getSearchField = (a) => a, { debounce = 300 } = {}) {
    return formCtrl.valueChanges
        .pipe(debounceTime(debounce), filter(_ => formCtrl.valid), startWith(''), map(searchValue => options.filter(contact => fuzzysearch(searchValue.toLowerCase(), getSearchField(contact).toLowerCase()))), share());
}
//# sourceMappingURL=fuzzysearch-reactive.js.map